import './App.css';
import Modal from './components/Modal/Modal';
import Items from './components/Items/Items';
import React, { Component } from 'react'

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      isActive: false,
      items:[],
      item:[],
    }
  }

  localStorageSetCart = () => {
    const {item} = this.state;
    const cartArr = JSON.parse(localStorage.getItem("Cart")) || [];

    if(cartArr.includes(item.id)){
      return
    }else{
      cartArr.push(item.id)
    }

    const cart = JSON.stringify(cartArr)
    localStorage.setItem("Cart",cart);
  }

  addToCart = () => {
    this.setState({isActive:false});
    this.localStorageSetCart();
  }

  inLocal = (items) => {
    const inLocal = JSON.parse(localStorage.getItem("Liked")) || [];
    const newArr = items.map(el => {
      if(inLocal.includes(el.id)){
        el.isFavorite = !el.isFavorite
      }
      return el
    })
    this.setState({items:newArr})
  }
  componentDidMount() { 
    fetch("http://localhost:3000/items.json", {
      method: "GET",
    }).then((res) => res.json()).then((data) => {
      this.setState({items:data})
      this.inLocal(data);
    });
    
  }

  openModal = (item) => {
    this.setState({item:item})
    this.setState({isActive:true});
  }

  closeModal = () => {
    this.setState({isActive:false});
  }

  localStorageSetLiked = (item) => {
    let likedArr = JSON.parse(localStorage.getItem("Liked")) || [];

    if(likedArr.includes(item.id)){
      const newlikedArr = likedArr.filter(el => el !== item.id);
      likedArr = newlikedArr;
    }else{
      likedArr.push(item.id)
    }

    const liked = JSON.stringify(likedArr)
    localStorage.setItem("Liked",liked);
  }

  fillSVG = (item) => {
    const {items} = this.state;
    const newArr = items.map(el => {
      if(el.id === item.id){
        el.isFavorite = !el.isFavorite
      }
      return el
    })

    this.setState({items:newArr})
    this.localStorageSetLiked(item)
  }

  render() {
    const {items,isActive} = this.state;
    return (
      <div className="App">
      {items.map(el => {
        return <Items key={el.id} items={el} fillSVG={this.fillSVG} onClick={this.openModal} />
      })}
      {isActive && <Modal header="Do you want to add this to cart?" text="" closeButton={true} onClick={this.closeModal} addToCart={this.addToCart}/>} 
    </div>
    )
  }
}
