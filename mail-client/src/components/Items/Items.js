import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import "./Items.scss"
import React, { Component } from 'react'

export default class Items extends Component {
    render() {
        const { fillSVG, onClick,items } = this.props
        const { items:{isFavorite,Url,title,id,price} } = this.props;
        return (
            <div className="card" id={id}>
            <img src={Url} alt="Img" width="230px" height="230px" />
            <h3>{title}</h3>
            <div className="card-footer">
                <p>{price}$</p>
                <Icon
                    onClick={() => fillSVG(items)}
                    color="#ffd700"
                    filled={isFavorite ? "#ffd700" : "none"}
                />
                <Button text="ADD TO CART" backgroundColor="black" className="openBtn" onClick={() => onClick(items)} />
            </div>
        </div>
        )
    }
}

