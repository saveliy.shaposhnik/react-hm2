import "./Button.scss";
import React, { Component } from 'react'

export default class Button extends Component {
    render() {
        const { text, backgroundColor, onClick, className } = this.props;
        return (
            <>
                <button style={{ backgroundColor: backgroundColor }} className={className} onClick={onClick}>{text}</button>
            </>
        )
    }
}


